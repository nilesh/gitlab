#! /bin/sh
# postrm.skeleton
# Skeleton maintainer script showing all the possible cases.
# Written by Charles Briscoe-Smith, March-June 1998.  Public Domain.

# Abort if any command returns an error value
set -e

# Set variables
gitlab_common_defaults_copy=/var/lib/gitlab-common/gitlab-common.defaults

# This script is called twice during the removal of the package; once
# after the removal of the package's files from the system, and as
# the final step in the removal of this package, after the package's
# conffiles have been removed.

# Ensure the menu system is updated

# Read debian specific configuration
if [ -f ${gitlab_common_defaults_copy} ]; then . ${gitlab_common_defaults_copy}; fi
if [ -f ${gitlab_common_conf} ]; then . ${gitlab_common_conf}; fi

case "$1" in
  remove)
    # This package is being removed, but its configuration has not yet
    # been purged.
    :

    # Remove diversion
    # ldconfig is NOT needed during removal of a library, only during
    # installation

    ;;
  purge)
    # This package has previously been removed and is now having
    # its configuration purged from the system.
    :
    # purge debconf questions
    if [ -e /usr/share/debconf/confmodule ]; then
      # Source debconf library.
      . /usr/share/debconf/confmodule

      if [ ! -z "${gitlab_user}" ]; then
        # Do only if gitlab_user is set
        echo "Removing user: ${gitlab_user}"
          if id -u ${gitlab_user}; then userdel -r ${gitlab_user}; fi
      else
        echo "gitlab_user not set. Hence not removing user."
      fi
      # Remove private copies of configuration files
      rm -f ${gitlab_common_conf_private}
      rm -f ${gitlab_common_defaults_copy}

      # Remove my changes to the db.
      db_purge
    fi

    # we mimic dpkg as closely as possible, so we remove configuration
    # files with dpkg backup extensions too:
    ### Some of the following is from Tore Anderson:
    for ext in '~' '%' .bak .ucf-new .ucf-old .ucf-dist;  do
	rm -f ${gitlab_common_conf}$ext
    done
 
    # Remove conf file
    if which ucf >/dev/null; then
      if [ -n "${gitlab_common_conf}" ]; then ucf --purge ${gitlab_common_conf}; fi
    fi
    if which ucfr >/dev/null; then
      if [ -n "${gitlab_common_conf}" ]; then ucfr --purge gitlab-common ${gitlab_common_conf}; fi
    fi
    rm -f ${gitlab_common_conf}
 
       # cleanup complete
    exit 0

    ;;
  disappear)
    if test "$2" != overwriter; then
      echo "$0: undocumented call to \`postrm $*'" 1>&2
      exit 0
    fi
    # This package has been completely overwritten by package $3
    # (version $4).  All our files are already gone from the system.
    # This is a special case: neither "prerm remove" nor "postrm remove"
    # have been called, because dpkg didn't know that this package would
    # disappear until this stage.
    :

    ;;
  upgrade)
    # About to upgrade FROM THIS VERSION to version $2 of this package.
    # "prerm upgrade" has been called for this version, and "preinst
    # upgrade" has been called for the new version.  Last chance to
    # clean up.
    :

    ;;
  failed-upgrade)
    # About to upgrade from version $2 of this package TO THIS VERSION.
    # "prerm upgrade" has been called for the old version, and "preinst
    # upgrade" has been called for this version.  This is only used if
    # the previous version's "postrm upgrade" couldn't handle it and
    # returned non-zero. (Fix old postrm bugs here.)
    :

    ;;
  abort-install)
    # Back out of an attempt to install this package.  Undo the effects of
    # "preinst install...".  There are two sub-cases.
    :

    if test "${2+set}" = set; then
      # When the install was attempted, version $2's configuration
      # files were still on the system.  Undo the effects of "preinst
      # install $2".
      :

    else
      # We were being installed from scratch.  Undo the effects of
      # "preinst install".
      :

    fi ;;
  abort-upgrade)
    # Back out of an attempt to upgrade this package from version $2
    # TO THIS VERSION.  Undo the effects of "preinst upgrade $2".
    :

    ;;
  *) echo "$0: didn't understand being called with \`$1'" 1>&2
     exit 0;;
esac

#DEBHELPER#
exit 0
