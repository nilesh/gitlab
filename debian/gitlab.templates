Template: gitlab/fqdn
Type: string
Default: localhost
_Description: Fully qualified domain name for this instance of Gitlab:
 Please choose the domain name which should be used to access this
 instance of Gitlab.
 .
 This should be the fully qualified name as seen from the Internet, with
 the domain name that will be used to access the Gitlab instance, without any
 protocol specifiers like https://.
 .
 If a reverse proxy is used, give the hostname that the proxy server
 responds to.
 .
 Example: git.example.com

Template: gitlab/ssl
Type: boolean
Default: false
_Description: Enable https?
 Enabling https means that an SSL/TLS certificate is required to access this
 Gitlab instance (as Nginx will be configured to respond only to https
 requests). A self-signed certificate is enough for local testing (and can be
 generated using, for instance, the package easy-rsa), but it is not
 recommended for a production instance.
 .
 Some certificate authorities like Let's Encrypt (letsencrypt.org), CAcert
 (cacert.org) offer free SSL/TLS certificates.
 Note: CAcert issued certificates are not trusted by all browsers, it requires
 installing CAcert's root certificate in such cases.
 .
 Nginx must be reloaded after the certificate and key files are made available
 at /etc/gitlab/ssl. letsencrypt package may be used to automate interaction
 with Let's Encrypt to obtain a certificate.

Template: gitlab/letsencrypt
Type: boolean
Default: false
_Description: Use Let's Encrypt?
 Symbolic links to certificate and key created using letsencrypt package
 (/etc/letencrypt/live) will be added to /etc/gitlab/ssl if this option is
 selected.
 .
 Otherwise, certificate and key files have to be placed manually to
 /etc/gitlab/ssl directory as 'gitlab.crt' and 'gitlab.key'.
 .
 Nginx will be stopped, if this option is selected, to allow letsencrypt to use
 ports 80 and 443 during domain ownership validation and certificate retrieval
 step.
 .
 Note: letsencrypt does not have a usable nginx plugin currently, so
 certificates must be renewed manually after 3 months, when current
 letsencrypt certificate expire. If you choose yes here, you will also be
 agreeing to letsencrypt terms of service.

Template: gitlab/letsencrypt_email
Type: string
_Description: Email address for letsencrypt updates:
 Please provide a valid email address for letsencrypt updates.

Template: gitlab/purge_data
Type: boolean
Default: true 
_Description: Remove all data?
 This will permanently remove all data of this Gitlab instance such as database,
 repositories, uploaded files, SSH public keys etc.
